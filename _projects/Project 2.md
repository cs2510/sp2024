---
layout: project
title: Project 2
subtitle: Replicated key-value data store
summary: |
  Replicated key-value data store with the help of ZooKeeper.
released: 23:59 Monday, March 18th, 2024.
due: 17:59 Friday, April 5th, 2024.
---

# Description

You will implement a replicated key-value data store maintained by N servers. Each server will maintain a copy of the data store and expose two functions

- Read(key): will read the value associated with the key.
- Add_Update(key, value): will add/update the value associated with the key.

A client may contact any of the server to read/add/update the data store. All Add/Update(key, value) requests will be routed to the leader server, who will be responsible for any write requests to the key-value. The leader will also be responsible for propagating the updates to other replicas. You will use ZooKeeper (a coordination service) to elect the leader (see more information below on how to use ZooKeeper for leader election).

All Read(key) request will be served as follows. If the key exists in the local data store, it will send the value even if it is “stale”. Else, it will return empty.

More info in the pdf. [Project 2 PDF]({{site.baseurl}}/projects/P2.pdf)
